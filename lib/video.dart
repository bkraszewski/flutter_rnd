import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:video_player/video_player.dart';

void logError(String code, String message) =>
    print('Error: $code\nError Message: $message');

class VideoApp extends StatefulWidget {
  List<CameraDescription> cameras;

  VideoApp(this.cameras);

  @override
  _VideoAppState createState() => _VideoAppState(cameras);
}

class _VideoAppState extends State<VideoApp> {
  List<CameraDescription> cameras;

  _VideoAppState(this.cameras);

  var _controllers = [];

  var _currentHorizontalPage = 1;

  var _currentVerticalPage = 0;
  CameraController _cameraController;

  @override
  void initState() {
    super.initState();

    _cameraController =
        new CameraController(cameras[0], ResolutionPreset.medium);
    _cameraController.initialize().then((_) {
      if (!mounted) {
        return;
      }
      setState(() {});
    });
    _currentVerticalPage = 0;

    var urls = [
      'https://firebasestorage.googleapis.com/v0/b/sniadan-13a9b.appspot.com/o/test-videos%2Fvideo1.mp4?alt=media&token=24e7aa1a-a139-44a5-812b-a412ce9d5e65',
      'https://firebasestorage.googleapis.com/v0/b/sniadan-13a9b.appspot.com/o/test-videos%2Fvideo2.mp4?alt=media&token=5de090ca-bca7-48d8-b18a-633ff74c5dfd',
      'https://firebasestorage.googleapis.com/v0/b/sniadan-13a9b.appspot.com/o/test-videos%2Fvideo3.mp4?alt=media&token=e6cb4cd4-ad4b-45c5-8313-08ff38d0c66b'
    ];

    _controllers = urls.map<VideoPlayerController>((item) {
      return VideoPlayerController.network(item)
        ..setLooping(true)
        ..initialize().then((_) {
          setState(() {});
          // Ensure the first frame is shown after the video is initialized, even before the play button has been pressed.
        });
    }).toList();

    setState(() {
      getCurrent().play();
    });
  }

  getCurrent() => _controllers[_currentVerticalPage];

  @override
  Widget build(BuildContext context) {

    return MaterialApp(
      title: 'Video Demo',
      home: Scaffold(
        body: PageView(
            onPageChanged: (index) {
              print('horizontal page changed to: $index');
              setState(() {
                _currentHorizontalPage = index;
                if (index == 0 || index == 2) {
                  pauseAllVideos();
                } else {
                  pauseAllVideos();
                  getCurrent().play();
                }
              });
            },
            children: <Widget>[
              !_cameraController.value.isInitialized
                  ? Container(color: Colors.pink)
                  : Center(
                      child: AspectRatio(
                          aspectRatio: _cameraController.value.aspectRatio,
                          child: CameraPreview(_cameraController))),
              PageView.builder(
                itemCount: _controllers.length,
                itemBuilder: (context, index) {
                  return Container(
                    child: Center(
                        child: _controllers[index].value.initialized
                            ? AspectRatio(
                                aspectRatio:
                                    _controllers[index].value.aspectRatio,
                                child: VideoPlayer(_controllers[index]),
                              )
                            : Container(color: Colors.brown)),
                  );
                },
                controller: PageController(
                    keepPage: true, initialPage: _currentVerticalPage),
                scrollDirection: Axis.vertical,
                onPageChanged: (index) {
                  setState(() {
                    print("vertical Page changed to $index ");
                    _currentVerticalPage = index;
                    pauseAllVideos();
                    getCurrent().play();
                  });
                },
              ),
              Container(
                color: Colors.deepPurple,
              ),
            ],
            controller: PageController(
                initialPage: _currentHorizontalPage, keepPage: true),
            scrollDirection: Axis.horizontal),
      ),
    );
  }

  void pauseAllVideos() {
    _controllers.forEach((controller) => controller.pause());
  }

  @override
  void dispose() {
    super.dispose();
    _cameraController?.dispose();
    _controllers.forEach((item) => item.dispose());
  }
}
